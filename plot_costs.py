#! /usr/bin/python
# coding: utf-8

import sys
import matplotlib.pyplot as plt
import numpy as np
import pylab
from matplotlib.font_manager import FontProperties

colors = [ '#cccccc', '#FF0000', '#E75500', '#E7C800', '#92E700', '#00FF00',
'#99E799', '#00E7C8', '#0092E7', '#001FE7', '#5500E7', '#000000', '#E70092',
'#777777']

all_methods = ['C45', 'Banerjee', 'Mod_prob_Err', 'Hybrid_C45', 'Hybrid_Mod_prob',
    'Hybrid_MetaCost', 'Cheapest_Class', 'Hybrid_Mod_prob_Lap', 'Mod_prob',
    'MetaCost', 'Hybrid_Mod_prob_Err', 'DSP_8', 'Hybrid_Laplace', 'Mod_prob_Lap',
    'Laplace']

methods_to_draw = ['Hybrid_Mod_prob', 'Cheapest_Class', 'Mod_prob']

if __name__ == "__main__":
    in_base = sys.argv[1]
    in_n = int(sys.argv[2])

    methods = {}
    for i in [j + 1 for j in range(in_n)]:
        in_file = open(in_base + str(i) + "/logs")
        for k, line in enumerate(in_file):
            if k >= 3:
                chunks = line.split()
                name = chunks[0]
                if not methods.has_key(chunks[0]):
                    methods[name]=[]
                try:
                    cost = float(chunks[1])
                except:
                    cost = None
                methods[name].append(cost)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    x = np.arange(100, 100 + in_n * 100, 100)
    for i, name in enumerate(methods.keys()):
        if name in methods_to_draw:
            ax.plot(x, methods[name], label=name, color=colors[i])

    # Appearance
    plt.xlabel('N') # TODO: not shown for some reason
    plt.ylabel('Vid. kaina')

    pylab.xlim([100, x[len(x) - 1]])
    font_p = FontProperties()
    font_p.set_size('small')

    # Shink current axis's height on the bottom
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.25, box.width, box.height * 0.75])

    # Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=3, prop=font_p)
    pylab.savefig('foo.png', bbox_inches=0, dpi=900)
    # pylab.show()
